# tearch-icon-theme
Papirus, Flat Remix and Qogir mix for TeArch.

## Dependencies
 - gtk-update-icon-cache
 - librsvg
 - curl _**(make)**_
 - git _**(make)**_

## Installation
You can install tearch-icon-theme by `make DESTDIR=/`.
