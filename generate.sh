#!/bin/bash
set -e
rm -rf ./build || true

mkdir -p build/mimetypes build/apps build/status build/devices build/animations build/panel || true

[[ -d sources/papirus ]] || git clone https://github.com/PapirusDevelopmentTeam/papirus-icon-theme --depth=1 sources/papirus
[[ -d sources/flat-remix ]] || git clone https://github.com/daniruiz/flat-remix --depth=1 sources/flat-remix
[[ -d sources/qogir ]] || git clone https://github.com/vinceliuice/Qogir-icon-theme --depth=1 sources/qogir

cp -rf sources/papirus/Papirus/48x48/apps/* build/apps/
cp -rf sources/papirus/Papirus/48x48/devices/* build/devices/
cp -rf sources/papirus/Papirus/48x48/status/* build/status/
cp -rf sources/papirus/Papirus/symbolic/* build
cp -rf sources/qogir/src/scalable/mimetypes/* build/mimetypes/
cp -rf sources/flat-remix/Flat-Remix-Blue-Dark/places/scalable/* build/places/
cp -rf sources/flat-remix/Flat-Remix-Blue-Dark/actions/scalable/* build/actions/
cp -rf sources/flat-remix/Flat-Remix-Blue-Dark/animations/* build/animations/
cp -rf sources/flat-remix/Flat-Remix-Blue-Dark/emblems/scalable/* build/emblems/

# Add Missing Icons
curl https://raw.githubusercontent.com/elementary/icons/master/status/symbolic/display-brightness-symbolic.svg > build/status/display-brightness-symbolic.svg
ln -sf display-brightness-symbolic.svg build/status/brightness-display-symbolic.svg
curl https://raw.githubusercontent.com/elementary/icons/master/status/symbolic/location-active-symbolic.svg > build/status/location-active-symbolic.svg
curl https://raw.githubusercontent.com/elementary/icons/master/status/symbolic/location-inactive-symbolic.svg > build/status/location-inactive-symbolic.svg
curl https://raw.githubusercontent.com/elementary/icons/master/actions/24/selection-checked.svg > build/actions/selection-checked.svg
curl https://raw.githubusercontent.com/elementary/icons/master/actions/symbolic/application-add-symbolic.svg > build/actions/application-add-symbolic.svg

cp index.theme build/

# Split themes
cp -r build build-pantheon
cp -rf sources/papirus/ePapirus/24x24/panel/* build-pantheon/panel/
cp -rf sources/papirus/Papirus/24x24/panel/* build/panel/
sed -i 's|=TeArch|=TeArch-Pantheon|g' build-pantheon/index.theme
