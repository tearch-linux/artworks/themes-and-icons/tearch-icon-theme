DESTDIR=/
all: build install

build:
	bash generate.sh

install:
	install -d $(DESTDIR)/usr/share/icons/TeArch/ || true
	cp -rf build/* $(DESTDIR)/usr/share/icons/TeArch/
	
	install -d $(DESTDIR)/usr/share/icons/TeArch-Pantheon/ || true
	cp -rf build-pantheon/* $(DESTDIR)/usr/share/icons/TeArch-Pantheon/

clean:
	rm -rf sources build build-pantheon
